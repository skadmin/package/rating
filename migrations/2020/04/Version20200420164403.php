<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200420164403 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'rating.overview', 'hash' => 'bd5a3f67e4ab4131d3cb27e4f996c0a2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Hodnocení', 'plural1' => '', 'plural2' => ''],
            ['original' => 'rating.overview.title', 'hash' => '419f2c2ff14b0bbdd6d78a333e6b3534', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Hodnocení|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.rating.overview.action.new', 'hash' => 'c9228fee1fa5ab1e84ec572b347085fe', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit hodnocení', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.rating.overview.name', 'hash' => '69e1ef23e969154086b3ca53c879ae80', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Autor', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.rating.overview.position', 'hash' => 'ac74518c953f2907ea663b9e6c9b36ec', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Funkce', 'plural1' => '', 'plural2' => ''],
            ['original' => 'rating.edit.title', 'hash' => '1a584a1ec4d857fa7340bf4e0dc17880', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení článku', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.rating.edit.name', 'hash' => '8b16c514408661747ca817caf1020570', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Autor', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.rating.edit.name.req', 'hash' => 'a5e990e7a23e17b91628c01e6280e1d4', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím autora', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.rating.edit.position', 'hash' => 'a0d282e40053d96f9fb3d73f326d0071', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Funkce', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.rating.edit.image-preview', 'hash' => 'b3aa7c2b9380f6278b81f1856464dfdd', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Profil autora', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.rating.edit.image-preview.rule-image', 'hash' => '6033b73d034f843689ba5dc633ae3e5c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.rating.edit.content', 'hash' => '5aa63a42cf09203a4d7ce504d474ff73', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Hodnocení', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.rating.edit.is-active', 'hash' => 'f34efc58a46771c00d0fdb5a506eee53', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.rating.edit.send', 'hash' => 'b2fe63e4d7835ca16053aa5754ffa458', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.rating.edit.send-back', 'hash' => '7561663cb53182f6f83bd0268e35cc51', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.rating.edit.back', 'hash' => '14bc99fbafb96de59c42f066bf791028', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'rating.edit.title - %s', 'hash' => '7b82c3efe0fef798ad3c96f631741aca', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace hodnocení', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.rating.edit.flash.success.create', 'hash' => '716326747a00ce036390536ad968609b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Hodnocení bylo úspěšně založeno.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.rating.edit.flash.success.update', 'hash' => 'e71f43527ce7a0434f7e3067dca9e176', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Hodnocení bylo úspěšně upraveno.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.rating.overview.action.edit', 'hash' => 'b2cb01dac6ac3eec563dcb2f45c066a3', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.rating.overview.is-active', 'hash' => '41091537b94242879f1aff1593de267d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.rating.title', 'hash' => '51e3430bcfa48bd105d6fc304e8adb55', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Hodnocení', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.rating.description', 'hash' => '43c173edcb5835b9039d8750471e7f70', 'module' => 'admin', 'language_id' => 1, 'singular' => 'umožňuje spravovat hodnocení', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.rating.overview.action.flash.sort.success', 'hash' => 'ba8245865506e5dc5a2d95d35c7ef1d8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Pořadí hodnocení bylo upraveno.', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
    }
}
