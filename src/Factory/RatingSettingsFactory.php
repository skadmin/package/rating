<?php

declare(strict_types=1);

namespace SkadminUtils\Rating\Factory;

class RatingSettingsFactory
{
    private bool $allowLink;

    public function __construct(bool $allowLink)
    {
        $this->allowVideo = $allowLink;
    }

    public function isAllowLink(): bool
    {
        return $this->allowVideo;
    }
}
