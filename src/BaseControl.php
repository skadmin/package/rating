<?php

declare(strict_types=1);

namespace Skadmin\Rating;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE  = 'rating';
    public const DIR_IMAGE = 'rating';

    public function getMenu(): ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-star-half-alt']),
            'items'   => ['overview'],
        ]);
    }
}
