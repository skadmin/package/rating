<?php

declare(strict_types=1);

namespace Skadmin\Rating\Doctrine\Rating;

use Doctrine\ORM\QueryBuilder;
use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits;
use SkadminUtils\DoctrineTraits\Facade;

final class RatingFacade extends Facade
{
    use DoctrineTraits\Facade\Sequence;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = Rating::class;
    }

    public function create(string $name, string $position, string $content, string $link, bool $isActive, ?string $imagePreview): Rating
    {
        return $this->update(null, $name, $position, $content, $link, $isActive, $imagePreview);
    }

    public function update(?int $id, string $name, string $position, string $content, string $link, bool $isActive, ?string $imagePreview): Rating
    {
        $rating = $this->get($id);
        $rating->update($name, $position, $content, $link, $isActive, $imagePreview);

        if (! $rating->isLoaded()) {
            $rating->setSequence($this->getValidSequence());
        }

        $this->em->persist($rating);
        $this->em->flush();

        return $rating;
    }

    public function get(?int $id = null): Rating
    {
        if ($id === null) {
            return new Rating();
        }

        $rating = parent::get($id);

        if ($rating === null) {
            return new Rating();
        }

        return $rating;
    }

    /**
     * @return Rating[]
     */
    public function getAll(bool $onlyActive = false): array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        $orderBy = ['sequence' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }

    public function getModel(?string $table = null): QueryBuilder
    {
        return parent::getModel($table)
            ->orderBy('a.sequence');
    }
}
