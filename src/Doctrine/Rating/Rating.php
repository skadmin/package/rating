<?php

declare(strict_types=1);

namespace Skadmin\Rating\Doctrine\Rating;

use Doctrine\ORM\Mapping as ORM;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
class Rating
{
    use Entity\Id;
    use Entity\Name;
    use Entity\Position;
    use Entity\Content;
    use Entity\IsActive;
    use Entity\ImagePreview;
    use Entity\Sequence;
    use Entity\Link;

    public function update(string $name, string $position, string $content, string $link, bool $isActive, ?string $imagePreview): void
    {
        $this->name     = $name;
        $this->position = $position;
        $this->content  = $content;
        $this->link     = $link;
        $this->isActive = $isActive;

        if ($imagePreview === null || $imagePreview === '') {
            return;
        }

        $this->imagePreview = $imagePreview;
    }
}
