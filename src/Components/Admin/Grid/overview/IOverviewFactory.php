<?php

declare(strict_types=1);

namespace Skadmin\Rating\Components\Admin;

interface IOverviewFactory
{
    public function create(): Overview;
}
