<?php

declare(strict_types=1);

namespace Skadmin\Rating\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\Rating\BaseControl;
use Skadmin\Rating\Doctrine\Rating\Rating;
use Skadmin\Rating\Doctrine\Rating\RatingFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use SkadminUtils\ImageStorage\ImageStorage;
use SkadminUtils\Rating\Factory\RatingSettingsFactory;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class Edit extends FormWithUserControl
{
    use APackageControl;

    private RatingSettingsFactory $ratingSettings;
    private LoaderFactory         $webLoader;
    private RatingFacade          $facade;
    private Rating                $rating;
    private ImageStorage          $imageStorage;

    public function __construct(?int $id, RatingSettingsFactory $ratingSettings, RatingFacade $facade, Translator $translator, LoaderFactory $webLoader, LoggedUser $user, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);
        $this->ratingSettings = $ratingSettings;

        $this->facade = $facade;

        $this->webLoader    = $webLoader;
        $this->imageStorage = $imageStorage;

        $this->rating = $this->facade->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->rating->isLoaded()) {
            return new SimpleTranslation('rating.edit.title - %s', $this->rating->getName());
        }

        return 'rating.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->imagePreview, BaseControl::DIR_IMAGE);

        if ($this->rating->isLoaded()) {
            if ($identifier !== null && $this->rating->getImagePreview() !== null) {
                $this->imageStorage->delete($this->rating->getImagePreview());
            }

            $rating = $this->facade->update(
                $this->rating->getId(),
                $values->name,
                $values->position,
                $values->content,
                $values->link,
                $values->isActive,
                $identifier
            );
            $this->onFlashmessage('form.rating.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $rating = $this->facade->create(
                $values->name,
                $values->position,
                $values->content,
                $values->link,
                $values->isActive,
                $identifier
            );
            $this->onFlashmessage('form.rating.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit',
            'id'      => $rating->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ]);
    }

    public function render(): void
    {
        $template               = $this->getComponentTemplate();
        $template->imageStorage = $this->imageStorage;
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/edit.latte');

        $template->ratingSettings = $this->ratingSettings;
        $template->rating         = $this->rating;
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.rating.edit.name')
            ->setRequired('form.rating.edit.name.req');
        $form->addText('position', 'form.rating.edit.position');
        $form->addCheckbox('isActive', 'form.rating.edit.is-active')
            ->setDefaultValue(true);
        $form->addImageWithRFM('imagePreview', 'form.rating.edit.image-preview');
        $form->addUrl('link', 'form.rating.edit.link', '(.*)');

        // TEXT
        $form->addTextArea('content', 'form.rating.edit.content');

        // BUTTON
        $form->addSubmit('send', 'form.rating.edit.send');
        $form->addSubmit('sendBack', 'form.rating.edit.send-back');
        $form->addSubmit('back', 'form.rating.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->rating->isLoaded()) {
            return [];
        }

        return [
            'name'     => $this->rating->getName(),
            'content'  => $this->rating->getContent(),
            'isActive' => $this->rating->isActive(),
            'position' => $this->rating->getPosition(),
            'link'     => $this->rating->getLink(),
        ];
    }
}
