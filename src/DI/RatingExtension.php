<?php

declare(strict_types=1);

namespace SkadminUtils\Rating\DI;

use Nette\DI\CompilerExtension;
use Nette\Schema\Expect;
use Nette\Schema\Schema;
use SkadminUtils\Rating\Factory\RatingSettingsFactory;

class RatingExtension extends CompilerExtension
{
    public function getConfigSchema(): Schema
    {
        return Expect::structure([
            'allowLink' => Expect::bool(false),
        ]);
    }

    public function loadConfiguration(): void
    {
        parent::loadConfiguration();

        $builder = $this->getContainerBuilder();
        $builder->addDefinition($this->prefix('ratingSettingsFactory'))
            ->setClass(RatingSettingsFactory::class)
            ->setArguments([
                $this->config->allowLink,
            ]);
    }
}
